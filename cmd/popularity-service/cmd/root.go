package cmd

import (
	"flag"
	"fmt"
	"github.com/juju/loggo"
	"github.com/soheilhy/cmux"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/popularity-service/pkg/apiserver"
	logutil "gitlab.com/mvenezia/popularity-service/pkg/util/log"
	"net"
	"os"
	"strings"
	"sync"
)

var (
	rootCmd = &cobra.Command{
		Use:   "popularity-service",
		Short: "Popularity Service API",
		Long:  `The Popularity Service API`,
		Run: func(cmd *cobra.Command, args []string) {
			runWebServer()
		},
	}
)

func init() {
	viper.SetEnvPrefix("popularityservice")
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)

	// using standard library "flag" package
	rootCmd.Flags().Int("port", 9050, "Port to listen on")

	viper.BindPFlag("port", rootCmd.Flags().Lookup("port"))

	viper.AutomaticEnv()
	rootCmd.Flags().AddGoFlagSet(flag.CommandLine)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func runWebServer() {
	logger := logutil.GetModuleLogger("cmd.cluster-manager-api", loggo.INFO)

	// get flags
	portNumber := viper.GetInt("port")

	var wg sync.WaitGroup
	stop := make(chan struct{})

	logger.Infof("Creating Web Server")
	tcpMux := createWebServer(&apiserver.ServerOptions{PortNumber: portNumber})
	wg.Add(1)
	go func() {
		defer wg.Done()
		logger.Infof("Starting to serve requests on port %d", portNumber)
		tcpMux.Serve()
	}()

	logger.Infof("Creating Secondary Server")
	tcpMux2 := createWebServer(&apiserver.ServerOptions{PortNumber: 8080})
	wg.Add(1)
	go func() {
		defer wg.Done()
		logger.Infof("Starting to serve requests on port %d", 8080)
		tcpMux2.Serve()
	}()

	<-stop
	logger.Infof("Wating for controllers to shut down gracefully")
	wg.Wait()
}

func createWebServer(options *apiserver.ServerOptions) cmux.CMux {
	conn, err := net.Listen("tcp", fmt.Sprintf(":%d", options.PortNumber))
	if err != nil {
		panic(err)
	}
	tcpMux := cmux.New(conn)

	apiserver.AddServersToMux(tcpMux, options)

	return tcpMux
}
