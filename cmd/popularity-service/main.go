package main

import "gitlab.com/mvenezia/popularity-service/cmd/popularity-service/cmd"

func main() {
	cmd.Execute()
}
