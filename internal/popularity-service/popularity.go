package popularityservice

import (
	pb "gitlab.com/mvenezia/popularity-service/pkg/generated/api"
	"golang.org/x/net/context"
	"math/rand"
)

func (s *Server) GetPopularity(ctx context.Context, in *pb.GetPopularityMsg) (*pb.GetPopularityReply, error) {
	SetLogger()
	result := rand.Float32() * 1000000
	return &pb.GetPopularityReply{Popularity: result}, nil
}
