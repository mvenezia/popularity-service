package popularityservice

import (
	"github.com/juju/loggo"
	"gitlab.com/mvenezia/popularity-service/pkg/util/log"
)

var (
	logger loggo.Logger
)

type Server struct{}

func SetLogger() {
	logger = log.GetModuleLogger("internal.popularity-service", loggo.INFO)
}
